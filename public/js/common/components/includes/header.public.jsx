import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

const mapStateToProps = (state) => {
	return {
		...state.oauth
	};
};

@connect(mapStateToProps, null)
class Header extends React.Component {
	static propTypes = {
		user: oneOfType([object])
	}

	static defaultProps = {
		user: {}
	}

    render() {
		const {
			user: {
				email
			}
		} = this.props;
        return (
			<header>
				<p>My email: {email}</p>
				<Link to="/logout">Logout</Link>
				<nav>
					<ul>
						<li>
							<Link to="/">
								Dashboard
							</Link>
						</li>
					</ul>
				</nav>
			</header>
        );
    }
}

export default Header;
