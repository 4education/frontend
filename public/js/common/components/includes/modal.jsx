import React, { Component } from 'react';
import {
    oneOfType,
    object,
    array,
	func,
	string
} from 'prop-types';

class Modal extends Component {
    static propTypes = {
		children: oneOfType([object, array]),
		onCancel: func,
		className: string
	}

	static defaultProps = {
        children: null,
		onCancel: null,
		className: ''
	}

	constructor(props) {
		super(props);

		this.state = {
			show: true
		};
	}

	hide() {
        const { show } = this.state;
		this.setState({ show: !show });
	}

    render() {
        const { children, onCancel, className } = this.props,
            { show } = this.state;

        return (show && (
                <div className="modal">
                    <div className={`modal__popup relative--core ${className}`}>
                        <div className="modal__cross cursor--pointer" data-action="close" onClick={onCancel || ::this.hide} role="presentation" />
                        {children}
                    </div>
                </div>
            ));
    }
}

export default Modal;
