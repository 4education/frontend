import React from 'react';
import uniqid from 'uniqid';
import {
    string,
    oneOfType,
    object,
    func
} from 'prop-types';
import moment from 'moment';

// components
import { DatePicker } from 'antd';

class Date extends React.Component {
    static propTypes = {
        input: oneOfType([object]),
        value: string,
        name: string,
        meta: oneOfType([object]),
        onChangeAction: func,
        wrapperClass: string,
        onChange: func
    }

    static defaultProps = {
        value: '',
        name: '',
        input: {},
        meta: {},
        onChangeAction: null,
        wrapperClass: '',
        onChange: null
    }

    constructor(props) {
        super(props);
        const {
            input: { value: inputValue },
            value,
            input: { name: inputName },
            name
        } = props;

        this.state = {
            value: inputValue || value,
            id: uniqid(`${(inputName || name)}-`)
        };
    }

    componentDidUpdate(prevProps) {
        const { input: { value: prevInputValue }, value: prevValue } = prevProps,
            { input: { value: nextInputValue }, value: nextValue } = this.props;

        if (prevInputValue !== nextInputValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: nextInputValue
            }));
            return false;
        }

        if (prevValue !== nextValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: nextValue
            }));
        }

        return false;
    }

    onChange(date, dateString) {
        const { onChange, name, input: { onChange: onChangeRedux } } = this.props;

        if (onChange) {
            onChange(name, moment(dateString).toISOString());
        }

        if (onChangeRedux) {
            onChangeRedux(moment(dateString).toISOString());
        }

        this.setState((prevState) => ({
            ...prevState,
            value: dateString
        }));
    }

    setError() {
        const { meta: { touched, error, warning } } = this.props;

        if (touched && (error || warning)) {
            return (
                <React.Fragment>
                    {
                        (error && <div className="message message--error">{error}</div>) ||
                        (warning && <div className="message message--error">{warning}</div>) ||
                        null
                    }
                </React.Fragment>
            );
        }

        return null;
    }

    render() {
        const {
            input: { name: reduxFormName },
            name,
            onChangeAction,
            wrapperClass
        } = this.props,
        { value } = this.state;

        return (
            <div className={wrapperClass}>
                <DatePicker
                    name={reduxFormName || name}
                    onChange={onChangeAction || ::this.onChange}
                    value={value ? moment(value) : null}
                    mode="date"
                />
                { this.setError() }
            </div>
        );
    }
}

export default Date;
