import React from 'react';
import uniqid from 'uniqid';
import { DatePicker } from 'antd';
import {
    string
} from 'prop-types';
const { RangePicker } = DatePicker;
import moment from 'moment';

class DateCalendar extends React.Component {
    static propTypes = {
        wrapClass: string
    }
    
    static defaultProps = {
        wrapClass: ''
    }
	
	constructor(props) {
        super(props);
        const {
            input: { value: inputValue },
            value,
            input: { name: inputName },
            name
        } = props;

        this.state = {
            value: inputValue || value,
            id: uniqid(`${(inputName || name)}-`)
        };
	}

	componentDidUpdate(prevProps) {
        const { input: { value: prevInputValue }, value: prevValue } = prevProps,
            { input: { value: nextInputValue }, value: nextValue } = this.props;

        if (prevInputValue !== nextInputValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: this.setToMoment(nextInputValue)
            }));
            return false;
        }

        if (prevValue !== nextValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: nextValue
            }));
        }

        return false;
	}
	
	setToMoment(value) {
		const arr = [];
		for (let index = 0; index < value.length; index++) {
			const element = value[index];
			arr.push(moment(element));
		}
		return arr;
	}

    onChange(date, dateArray) {
        const { onChange, name, input: { onChange: onChangeRedux } } = this.props;

        if (onChange) {
            onChange(name, dateArray);
        }

        if (onChangeRedux) {
            onChangeRedux(dateArray);
        }

        this.setState((prevState) => ({
            ...prevState,
            value: date
        }));
	}
	
	setError() {
        const { meta: { touched, error, warning } } = this.props;

        if (touched && (error || warning)) {
            return (
                <React.Fragment>
                    {
                        (error && <div className="message message--error">{error}</div>) ||
                        (warning && <div className="message message--error">{warning}</div>) ||
                        null
                    }
                </React.Fragment>
            );
        }

        return null;
    }

    render() {
        const {
            input: { name: reduxFormName },
            name,
            onChangeAction,
            wrapClass
        } = this.props,
		{ value } = this.state;
		
        return (
            <div className={`${wrapClass ? wrapClass : ''}`}>
				<RangePicker 
					name={reduxFormName || name}
					onChange={onChangeAction || ::this.onChange}
					value={value ? value : null}
				/>
				{ this.setError() }
            </div>
        );
    }
}

export default DateCalendar;
