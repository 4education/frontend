import React from 'react';
import { Checkbox } from 'antd';
import {
	oneOfType,
	array,
	object,
	bool,
	string
} from 'prop-types';

class CheckboxGroupAnt extends React.Component {
	static propTypes = {
		input: oneOfType([object]),
		options: oneOfType([array]).isRequired,
		defaultValue: oneOfType([array]),
		disabled: bool,
		name: string,
		meta: oneOfType([object]),
		wrapClass: string
	}

	static defaultProps = {
		disabled: false,
		defaultValue: [],
		input: {},
		meta: {},
		name: '',
		wrapClass: ''
	}

	constructor(props) {
		super(props);
		const {
				input: {
					value: inputValue
				},
				value
			} = props;

		this.state = {
			value: inputValue || value
		};
	}


	componentDidUpdate(prevProps) {
		const { input: { value: prevInputValue }, value: prevValue } = prevProps,
			  { input: { value: nextInputValue }, value: nextValue } = this.props;

		if (prevInputValue !== nextInputValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextInputValue
			}));
			return false;
		}

		if (prevValue !== nextValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextValue
			}));
		}

		return false;
	}

	onChange = (value) => {
		const { input: { onChange: onInputReduxChange }, onChange } = this.props,
				val = value && value.length ? value : '';

		 this.setState((prevState) => ({
			...prevState,
			value
		}));

		if (onInputReduxChange) {
			onInputReduxChange(val);
		}

		if (onChange) {
			onChange(val);
		}
	}

	normalizeValue() {
		const { options, defaultValue } = this.props,
				values = [];

		if (defaultValue && defaultValue.length) {
			return defaultValue;
		}

		for (const variable of options) {
			if (variable.checked) {
				values.push(variable.value);
			}
		}

		return values;
	}

	render() {
		const {
			name,
			disabled,
			options,
			wrapClass,
			meta: { touched, error, warning }
		} = this.props,
		defaults = this.normalizeValue(),
		{ value } = this.state;

		return (
			<div className={`relative--core ${wrapClass}`}>
				<Checkbox.Group
					options={options}
					name={name}
					defaultValue={defaults}
					onChange={this.onChange}
					value={value || defaults || []}
					disabled={disabled}
				/>
				{touched &&
					((error && <span className="unclassed--error text text--preview clr--red padding--t-10 inline-block--core absolute--core">{error}</span>) ||
					  (warning && <span className="unclassed--warning text text--preview clr--red padding--t-10 inline-block--core absolute--core">{warning}</span>))}
			</div>
		);
	}
}

export default CheckboxGroupAnt;
