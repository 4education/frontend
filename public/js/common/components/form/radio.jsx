import React from 'react';
import { Radio } from 'antd';
import uniqid from 'uniqid';
import {
	oneOfType,
	array,
	bool,
	string,
	object
} from 'prop-types';

class RadioGroupComponent extends React.Component {
	static propTypes = {
		options: oneOfType([array]).isRequired,
		input: oneOfType([object]),
		meta: oneOfType([object]),
		defaultValue: oneOfType([array]),
		groupClass: string,
		wrapClass: string,
		disabled: bool
	}

	static defaultProps = {
		groupClass: '',
		wrapClass: '',
		disabled: false,
		defaultValue: [],
		input: {},
		meta: {}
	}

	constructor(props) {
		super(props);
		const {
				input: {
					value: inputValue
				},
				value
			} = props;

		this.state = {
			value: inputValue || value
		};
	}


	componentDidUpdate(prevProps) {
		const { input: { value: prevInputValue }, value: prevValue } = prevProps,
			  { input: { value: nextInputValue }, value: nextValue } = this.props;

		if (prevInputValue !== nextInputValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextInputValue
			}));
			return false;
		}

		if (prevValue !== nextValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextValue
			}));
		}

		return false;
	}

	__onChange = (value) => {
		const { input: { onChange: onInputReduxChange }, onChange } = this.props;

		 this.setState((prevState) => ({
			...prevState,
			value
		}));

		if (onInputReduxChange) {
			onInputReduxChange(value);
		}

		if (onChange) {
			onChange(value);
		}
	}

	normalizeValue() {
		const { options, defaultValue } = this.props,
				values = [];

		if (defaultValue && defaultValue.length) {
			return defaultValue;
		}

		for (const variable of options) {
			if (variable.checked) {
				values.push(variable.value);
			}
		}

		return values;
	}

	render() {
		const {
			input: { name },
			disabled,
			options,
			groupClass,
			wrapClass, 
			meta: { touched, error, warning }
		} = this.props,
		defaults = this.normalizeValue(),
		{ value } = this.state;

		return (
			<div className={`relative--core ${wrapClass}`}>
				<Radio.Group
					disabled={disabled}
					name={name}
					defaultValue={defaults}
					onChange={this.__onChange}
					value={value || defaults || []}
					className={groupClass}
				>
					{options.map((item) => (
						<Radio
							key={`${item.label}-${uniqid()}`}
							value={item.value}
							disabled={item.disabled}
						>
							{item.label}
						</Radio>
					))}
				</Radio.Group>
				{touched &&
					((error && <span className="unclassed--error text text--preview clr--red padding--t-10 inline-block--core absolute--core">{error}</span>) ||
					  (warning && <span className="unclassed--warning text text--preview clr--red padding--t-10 inline-block--core absolute--core">{warning}</span>))}
			</div>
		);
	}
}

export default RadioGroupComponent;
