import React from 'react';
import {
    string,
    func,
    oneOfType,
    object
   } from 'prop-types';

class Textarea extends React.Component {
    static propTypes = {
        className: string,
        placeholder: string,
        rows: string,
        value: string,
        input: oneOfType([object]),
        meta: oneOfType([object]),
        onChange: func,
        onBlur: func
    }

    static defaultProps = {
        className: '',
        placeholder: '',
        rows: '',
        value: '',
        input: {},
        meta: {},
        onChange: () => {},
        onBlur: () => {}
    }

    constructor(props) {
        super(props);

        const { input: { value: inputValue }, value } = props;
        this.state = {
            value: inputValue || value
        };
    }

    componentDidUpdate(prevProps) {
        const { input: { value: prevInputValue }, value: prevValue } = prevProps,
              { input: { value: nextInputValue }, value: nextValue } = this.props;

        if (prevInputValue !== nextInputValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: nextInputValue
            }));
            return false;
        }

        if (prevValue !== nextValue) {
            this.setState((prevState) => ({
                ...prevState,
                value: nextValue
            }));
        }

        return false;
    }

    __onChange = (event) => {
        const { input: { onChange: onInputReduxChange }, onChange } = this.props,
              { target: { value } } = event;

        this.setState((prevState) => ({
            ...prevState,
            value
        }));

        if (onInputReduxChange) {
            onInputReduxChange(value);
        }

        if (onChange) {
            onChange(value);
        }
    }

    __onBlur = (event) => {
        const { input: { onBlur: onTextAreaReduxBlur }, onBlur } = this.props,
              { target: { value } } = event;

         if (onTextAreaReduxBlur) {
             onTextAreaReduxBlur(value);
         }

         if (onBlur) {
             onBlur(value);
         }
    }

    render() {
        const {
            className,
            placeholder,
            rows,
            meta: { touched, error, warning }
        } = this.props,
        { value } = this.state;

        return (
            <div>
                <textarea
                  className={className}
                  placeholder={placeholder}
                  rows={rows}
                  onChange={this.__onChange}
                  onBlur={this.__onBlur}
                  value={value}
                />
                {touched &&
                    ((error && <span className="unclassed--error text text--preview clr--red padding--t-10 margin--l-25 inline-block--core absolute--core">{error}</span>) ||
                      (warning && <span className="unclassed--warning text text--preview clr--red padding--t-10 margin--l-25 inline-block--core absolute--core">{warning}</span>))}
            </div>
        );
    }
}

export default Textarea;
