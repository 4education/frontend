import React from 'react';
import { Select, Spin } from 'antd';
import {
	oneOfType,
	object,
	string,
	func,
	bool,
	array,
	number
} from 'prop-types';

import 'antd/dist/antd.css';
import '../../../../styl/blocks/select.styl';

import uniqid from 'uniqid';

class SelectBox extends React.Component {
	static propTypes = {
		input: oneOfType([object]),
		name: string,
		options: oneOfType([array]),
		placeholder: string,
		onChange: func,
		search: bool,
		dropdownClass: string,
		disabled: bool,
		wrapClass: string,
		mode: string,
		className: string,
		showArrow: bool,
		defaultValue: string,
		value: oneOfType([array, string, number])
	}

	static defaultProps = {
		options: [],
		name: '',
		placeholder: '',
		onChange: null,
		disabled: false,
		search: false,
		dropdownClass: '',
		wrapClass: '',
		mode: 'default',
		input: {},
		value: '',
		defaultValue: '',
		showArrow: true,
		className: ''
	}

	constructor(props) {
		super(props);
		this.state = {
			fetching: false,
			value: undefined
		};
	}

	componentDidMount() {
		const { input: { value: inputValue }, value } = this.props;
		this.setState((prevState) => ({
			...prevState,
			value: inputValue || value
		}));
	}

	componentDidUpdate(prevProps) {
		const { input: { value: prevInputValue }, value: prevValue } = prevProps,
			  { input: { value: nextInputValue }, value: nextValue } = this.props;

		if (prevInputValue !== nextInputValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextInputValue
			}));
			return false;
		}

		if (prevValue !== nextValue) {
			this.setState((prevState) => ({
				...prevState,
				value: nextValue
			}));
		}

		return false;
	}

	onChange = (value) => {
		const { input: { onChange: onInputReduxChange }, onChange, name } = this.props;

		  if (onInputReduxChange) {
			  onInputReduxChange(value);
		  }

		  if (onChange) {
			  onChange(value, name);
		  }
		  
		  this.setState((prevState) => ({
			...prevState,
			value
		}));
	}

	setFetching() {
		this.setState = {
			fetching: true
		};
	}

	verifyValue(value) {
		const { options, mode, defaultValue } = this.props;
		if (mode === 'multiple') {
			return value;
		} else if (mode === 'default') {
			const vl = Array.isArray(value) ? _.head(value) : value,
				item = _.find(options, { key: vl }) ? vl : null;

			if (!item && defaultValue) {
				return defaultValue;
			}

			return item;
		}
	}

	render() {
		const {
			options,
			placeholder,
			className,
			search,
			dropdownClass,
			disabled,
			wrapClass,
			showArrow,
			mode
		} = this.props,
		{ fetching, value } = this.state,
		{ Option } = Select,
		val = this.verifyValue(value);

		return (
			<React.Fragment>
				<div className={`relative--core width--100 ${wrapClass}`}>
					<Select
					  mode={mode}
					  showSearch={search}
					  onChange={this.onChange}
					  placeholder={placeholder}
					  className={className}
					  showArrow={showArrow}
					  disabled={disabled}
					  dropdownClassName={dropdownClass}
					  value={val || undefined}
					  notFoundContent={fetching ? <Spin size="small" /> : <p>Not found</p>}
					>
						{
							options.map((item) => {
								const { key: optionKey, name: optionName } = item;
								return (
									<Option key={`${optionKey}-${uniqid()}`} value={optionKey}>
										{optionName}
									</Option>
								);
							})
						}
					</Select>
				</div>
			</React.Fragment>
		);
	}
}

export default SelectBox;
