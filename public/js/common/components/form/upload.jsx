import React from 'react';
import {
    string,
    func,
    oneOfType,
    object
   } from 'prop-types';

import { isBase64 } from '../../../helpers/isBase64.helper.js';

import configuration from '../../../configuration/host.js';

const {
	protocol,
	fileStorageService
} = configuration;

class Upload extends React.Component {
    static propTypes = {
        input: oneOfType([object]),
		meta: oneOfType([object]),
		onChange: func,
		value: string,
		name: string
    }

    static defaultProps = {
        input: {},
		meta: {},
		onChange: null,
		value: '',
		name: ''
    }

    constructor(props) {
        super(props);

        const { input: { value: inputValue }, value } = props;
        this.state = {
            value: inputValue || value
        };
	}

	componentDidMount() {
		const {
			value
		} = this.state;
		this.__onLoadImages(value);
	}

    componentDidUpdate(prevProps) {
        const { input: { value: prevInputValue }, value: prevValue } = prevProps,
              { input: { value: nextInputValue }, value: nextValue } = this.props;

        if (prevInputValue !== nextInputValue) {
            this.setState(prevState => ({
                ...prevState,
                value: nextInputValue
			}));
			this.__onLoadImages(nextInputValue);
            return false;
        }

        if (prevValue !== nextValue) {
            this.setState(prevState => ({
                ...prevState,
                value: nextValue
			}));
			this.__onLoadImages(nextValue);
        }

        return false;
	}

    __onChange = (event) => {
		const { target: { files } } = event,
			  { input: { onChange: onInputReduxChange }, onChange } = this.props,
				file = _.head(files),
				reader = new FileReader();

		reader.onloadend = () => {
			this.setState(prevState => ({
				...prevState,
				value: [reader.result],
				src: reader.result
			}));

			if (onInputReduxChange) {
				onInputReduxChange([reader.result]);
			}

			if (onChange) {
				onChange([reader.result]);
			}
		};

		if (file) {
			reader.readAsDataURL(file);
		}
	}

	__onLoadImages(value) {
		if (!value) return;

		const src = Array.isArray(value) ? _.head(value) : value,
			  img = new Image(),
			  url = isBase64(src) ? src : `${protocol}${fileStorageService}/${src}`;

		img.src = url;

        img.onload = () => {
            this.setState(prevState => ({
				...prevState,
				src: url
			}));
        };
        img.onerror = () => {
			this.setState(prevState => ({
				...prevState,
				src: '/img/missing.png'
			}));
        };
    }


    render() {
        const {
			meta: { touched, error, warning },
			input: { name: reduxName },
			name
        } = this.props,
        {
			value,
			src
		} = this.state;

        return (
			<div>
				<label
					htmlFor={name || reduxName}
					style={{ display: 'block', padding: '20px', border: '1px solid green' }}
				>
					{
						value && !src ? (
							<p>Loading...</p>
						) : value && src ? (
							<img width="200" src={src} alt="Upload screen logo" />
						) : (
							<p>Upload</p>
						)
					}
					<input
						onChange={this.__onChange}
						style={{ position: 'absolute', visibility: 'hidden', left: '-99999px' }}
						id={name || reduxName}
						type="file"
					/>
				</label>
                {touched &&
                    ((error && <span className="unclassed--error text text--preview clr--red padding--t-10 margin--l-25 inline-block--core absolute--core">{error}</span>) ||
                      (warning && <span className="unclassed--warning text text--preview clr--red padding--t-10 margin--l-25 inline-block--core absolute--core">{warning}</span>))}
            </div>
        );
    }
}

export default Upload;
