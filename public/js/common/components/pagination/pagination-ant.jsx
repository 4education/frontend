import React from 'react';
import { Pagination } from 'antd';
import {
    bool,
    number,
    instanceOf,
    oneOfType,
    func
} from 'prop-types';

class PaginationAnt extends React.Component {
    static itemRender(current, type, originalElement) {
         if (type === 'prev') {
           return <div className={`icon-fontello icon-fontello--medium icon-left-dir cursor--pointer ${current === 0 ? 'clr--gray' : 'hover--iblue clr--blue'}`} />;
         }
         if (type === 'next') {
           return <div className="icon-fontello icon-fontello--medium icon-right-dir cursor--pointer clr--blue hover--iblue" />;
         }
         return originalElement;
     }

    static propTypes = {
        total: number.isRequired,
        disabled: bool,
        pageSize: number.isRequired,
        current: number.isRequired,
        onChange: oneOfType([func, instanceOf(null)])
    }

    static defaultProps = {
        disabled: false,
        onChange: null
    }

    __onChange = (value) => {
        const { onChange } = this.props;

        if (onChange) {
            onChange(value);
        }
    }

    render() {
        const {
            pageSize,
            total,
            disabled,
            current
        } = this.props;

        return (
            <Pagination
              onChange={this.__onChange}
              total={total}
              current={current}
              itemRender={PaginationAnt.itemRender}
              disabled={disabled}
              pageSize={pageSize}
            />
        );
    }
}

export default PaginationAnt;
