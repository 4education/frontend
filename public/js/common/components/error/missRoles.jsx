import React from 'react';
import { Link } from 'react-router-dom';

class MissRoles extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className="">
                    <h1 className="" title="Error 401">
                        Error 401
                    </h1>
                    <Link className="" to="/">
                        Sing in
                    </Link>
                </div>
            </React.Fragment>
        );
    }
}

export default MissRoles;
