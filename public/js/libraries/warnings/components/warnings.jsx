import React from 'react';
import { oneOfType, object } from 'prop-types';

import '../../../../styl/blocks/warnings.styl';

class Warnings extends React.Component {
	static propTypes = {
		warnings: oneOfType([object]),
		i18n: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		warnings: {},
		i18n: {}
	}

	constructor(props) {
		super(props);
		this.__refs = null;
	}

	componentDidMount() {
		document.addEventListener('mousedown', this.__handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.__handleClickOutside);
	}

	__onClose = () => {
		const { actions: { warningSetter } } = this.props;

		warningSetter(null);
	}

	__handleClickOutside = (event) => {
		if (this.__refs && !this.__refs.contains(event.target)) {
			this.__onClose();
		}
	}

	staticNotice(message, type) {
		return (
			<div ref={(refs) => { this.__refs = refs; }} className={`warnings fl fl-align-c fl-justify-st warnings--${type}`}>
				<button type="button" aria-label="close" aria-hidden="true" onClick={this.__onClose} className="icon-fontello icon-fontello--small icon-cancel-circled clr--white" />
				<p className="warnings__text" dangerouslySetInnerHTML={{ __html: message }} />
			</div>
		);
	}

	renderNotice() {
		const { 
				warnings: { 
					type, 
					message, 
					code 
				}, 
				i18n, 
				i18n: { 
					translation: {
						button: {
							plans
						} = {},
						notification: {
							oops
						} = {}
					} = {}
				} } = this.props,
			msg = message;

		return this.staticNotice(msg, type);
	}

    render() {
        return this.renderNotice();
    }
}

export default Warnings;
