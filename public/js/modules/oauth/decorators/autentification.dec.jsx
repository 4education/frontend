import React from 'react';
import {
	oneOfType,
	object
} from 'prop-types';

import {
	Route,
	Switch
} from 'react-router-dom';

// Routers
import routes from '../routes/oauth.router.js';
import Error from '../../../common/components/error/error.jsx';

const AutentificationDecorator = (WrapedComponent) => {
	class Autentification extends React.Component {
		static propTypes = {
			oauth: oneOfType([object]).isRequired,
			history: oneOfType([object]).isRequired
		}

		constructor(props, context) {
			super(props, context);
			const { oauth: { isAuthenticating } } = props;
			this.state = { autentification: isAuthenticating };
		}

		componentDidUpdate(prevProps) {
			const { autentification: currentAutentification } = this.state,
				  { oauth: { isAuthenticating: currentIsAuthenticating }, history: { push } } = this.props,
				  { oauth: { isAuthenticating: prevIsAuthenticating } } = prevProps;

			if (
				!currentAutentification &&
				currentIsAuthenticating
			) {
				push('/');
				this.setState({ autentification: currentIsAuthenticating });
			}

			if (
				!currentIsAuthenticating &&
				currentIsAuthenticating !== prevIsAuthenticating
			) {
				push('/');
				this.setState({ autentification: false });
			}
		}

		render() {
			const { autentification } = this.state;
			if (autentification) {
				return (
					<WrapedComponent {...this.props} />
				);
			}

			return (
				<Switch>
					{
						routes && routes.length && routes.map((item) => {
							const { path, component } = item;
							return (
								<Route key={path} exact path={path} component={component} />
							);
						})
					}
					<Route component={Error} />
				</Switch>
			);
		}
	}
	return Autentification;
};

export default AutentificationDecorator;
