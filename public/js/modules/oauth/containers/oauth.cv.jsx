import React, { Suspense } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as authorization from '../actions/oauth.action.js';
import EventHandling from '../../../libraries/warnings/index.jsx';

const mapStateToProps = (state) => {
	return {
		...state.oauth,
		realTimeForm: state.form,
		warnings: state.warnings
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...authorization
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class Login extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired,
		realTimeForm: oneOfType([object]),
		warnings: oneOfType([object])
	}

	static defaultProps = {
		realTimeForm: {},
		warnings: null
	}

	constructor(props) {
		super(props);
		this.state = {
			components: {}
		};
	}

	componentDidMount() {
		const {
			match: {
				params: {
					token
				}
			},
			actions: {
				activate
			}
		} = this.props;

		if (token) {
			activate(token);
		}

		this.components();
	}

	componentDidUpdate(prevProps, prevState) {
		const { active: currentActive } = this.state,
			  { active: prevAcive } = prevState;

		if (!currentActive && currentActive !== prevAcive) {
			this.components();
		}
	}

	async components() {
		const {
			match: {
				params: {
					type
				}
			}
		} = this.props,
		{
			components,
			active: currentActive
		} = this.state;

		let mdl = null,
			active = null;

		switch (type) {
			case 'signup':
				mdl = React.lazy(() => import('../components/signup.jsx'));
				active = 'signup';
				break;
			default:
				mdl = React.lazy(() => import('../components/login.jsx'));
				active = 'signin';
		}

		if (!components[active]) {
			this.setState(prevState => ({
				...prevState,
				components: {
					...prevState.components,
					[active]: mdl
				},
				active
			}));
		} else if (currentActive !== active) {
			this.setState(prevState => ({
				...prevState,
				active
			}));
		}

		return false;
	}

	render() {
		const { active } = this.state,
			  { warnings } = this.props;

		if (!active) return null;

		const { components: { [active]: Component } } = this.state;

		return (
			<React.Fragment>
				<EventHandling warnings={warnings} />
				{
					Component ? ( 
						<Suspense fallback={<div>Загрузка...</div>}>
							<Component {...this.props} /> 
						</Suspense>
					) : null
				}
			</React.Fragment>
		);
	}
}

export default Login;
