import React, { Suspense } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    oneOfType,
    object
} from 'prop-types';

// Actions
import * as authorization from '../actions/oauth.action.js';

// Components
import EventHandling from '../../../libraries/warnings/index.jsx';

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            ...authorization
        }, dispatch),
        dispatch
    };
}

@connect(null, mapDispatchToProps)
class Invitation extends React.Component {
    static propTypes = {
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired,
		history: oneOfType([object]).isRequired,
		warnings: oneOfType([object])
	}

	static defaultProps = {
		warnings: {}
	}

	constructor(props) {
		super(props);
		const Component = React.lazy(() => import('../components/invitation.jsx'));
		this.state = {
			component: Component
		};
	}

	componentDidMount() {
		const {
			match: {
				params: {
					token
				}
			},
			history: {
				push
			}
		} = this.props;

		if (!token) {
			push('/');
		}
	}

    render() {
		const { component: Component } = this.state,
			  { warnings } = this.props;

        return (
			<React.Fragment>
				<EventHandling warnings={warnings} />
				{
					Component ? (
						<Suspense fallback={<div>Загрузка...</div>}>
							<Component {...this.props} />
						</Suspense>
					) : null
				}
			</React.Fragment>
		);
    }
}

export default Invitation;
