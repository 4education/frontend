import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    oneOfType,
    object
} from 'prop-types';

// Actions
import * as authorization from '../actions/oauth.action.js';

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            ...authorization
        }, dispatch),
        dispatch
    };
}

@connect(null, mapDispatchToProps)
class Logout extends React.Component {
    static propTypes = {
        actions: oneOfType([object]).isRequired
    }

	componentDidMount() {
		const {
			actions: {
				logout
			}
		} = this.props;

		logout();
	}

    render() {
        return null;
    }
}

export default Logout;
