import jwt from 'jwt-decode';
import { LOG_IN, LOG_OUT } from '../constants/oauth.const.js';
import { SET_WARNINGS } from '../../../libraries/warnings/constant/warnings.const.js';
import Api from '../../../service/api.js';

export function registration(data) {
    return async (dispatch) => {
		try {
			const { url } = data;
			let path = '';

			switch (url) {
				case 'personal':
					path = 'request/registration';
					break;
				default:
					path = 'request/registration/company';
					break;
			}

			data.service = 'educational';
			await Api.__request({
				url: path,
				method: 'POST',
				data,
				type: 'login'
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: 'We have sent the activation link by email.'
				}
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function activate(token, data = {}) {
    return async (dispatch) => {
		try {
			const { userData, token: bToken } = await Api.__request({
				url: `approve/registration/${token}`,
				method: 'POST',
				data,
				type: 'login'
			});

			if (userData && bToken) {
				localStorage.authToken = bToken;

				dispatch({
					type: LOG_IN,
					 user: userData
				});
			} else {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'error',
						message: 'User was not received. Try to signup again!'
					}
				});
			}
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function login(data) {
    return async (dispatch) => {
		try {
			data.service = 'educational';
			const { token } = await Api.__request({
				url: 'login/local',
				method: 'POST',
				data,
				type: 'login'
			});

			if (token) {
				localStorage.authToken = token;

				dispatch({
					type: LOG_IN,
					user: jwt(token)
				});
			} else {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'error',
						message: 'We can`t allow this action. Try to singup'
					}
				});
			}
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function logout() {
    return (dispatch) => {
        dispatch({
            type: LOG_OUT
        });
    };
}
