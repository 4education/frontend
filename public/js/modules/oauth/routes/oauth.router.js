import Login from '../containers/oauth.cv.jsx';
import Invitation from '../containers/invitation.cv.jsx';

const login = [
	{
		path: '/:type?',
		component: Login
	},
	{
		path: '/activate/:token',
		component: Login
	},
	{
		path: '/invite/:token',
		component: Invitation
	}
];

export default login;
