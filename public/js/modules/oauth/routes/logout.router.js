import Logout from '../containers/logout.cv.jsx';

export const logout = [
	{
		path: '/logout',
		component: Logout
	}
];
