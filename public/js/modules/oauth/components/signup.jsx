import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { func, object, oneOfType } from 'prop-types';

import Input from '../../../common/components/form/input.jsx';
import Select from '../../../common/components/form/select.jsx';

import { required, email, samePassword } from '../../../helpers/validation.helper.js';

@reduxForm({
	form: 'sign',
	enableReinitialize: true,
	initialValues: {
		url: 'company'
	}
})
class SignUp extends React.Component {
	static propTypes = {
		handleSubmit: func.isRequired,
		actions: oneOfType([object]).isRequired,
		realTimeForm: oneOfType([object])
	}

	static defaultProps = {
		realTimeForm: {}
	}

	onSubmit(values) {
		const {
			actions: {
				registration
			}
		} = this.props;

		registration(values);
	}

	render() {
		const {
			handleSubmit,
			realTimeForm: {
				sign: {
					values: {
						url
					} = {}
				} = {}
			}
		} = this.props;
		return (
			<form onSubmit={handleSubmit(::this.onSubmit)}>
				<div className="margin--b-30">
					<Field
						component={Select}
						validate={[required]}
						label="Email"
						name="url"
						options={[
							{ key: 'company', label: 'company', name: 'Company' },
							{ key: 'personal', label: 'company', name: 'Personal' }
						]}
						className="select"
						placeholder="Type"
					/>
				</div>
				{
					!url || url === 'company' ? (
						<div className="margin--b-30">
							<Field
								component={Input}
								validate={[required]}
								label="Comany name"
								name="company"
								className="input"
								autoComplete="no-company"
								placeholder="Comany name"
							/>
						</div>
					) : null
				}
				<div className="margin--b-30">
					<Field
						component={Input}
						validate={[required]}
						label="First and Last name"
						name="name"
						className="input"
						autoComplete="no-name"
						placeholder="First and Last name"
					/>
				</div>
				<div className="margin--b-30">
					<Field
						component={Input}
						validate={[required, email]}
						label="Email"
						name="email"
						className="input"
						autoComplete="no-password"
						placeholder="Email adress"
					/>
				</div>
				<div className="margin--b-30">
					<Field
						component={Input}
						name="password"
						validate={[required]}
						className="input"
						type="password"
						placeholder="Password"
					/>
				</div>
				<div className="form-group">
					<Field
						component={Input}
						name="passwordRepeat"
						validate={[required, samePassword]}
						className="input"
						type="password"
						placeholder="Repeat password"
					/>
				</div>
				<button type="submit">Sign ups</button>
			</form>
		);
	}
}

export default SignUp;
