import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { func, oneOfType, object } from 'prop-types';

import Input from '../../../common/components/form/input.jsx';

import { required, email } from '../../../helpers/validation.helper.js';

@reduxForm({ form: 'sign', enableReinitialize: true })
class LoginForm extends React.Component {
	static propTypes = {
		handleSubmit: func.isRequired,
		realTimeForm: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		realTimeForm: {}
	}

	onSubmit(values) {
		const {
			actions: {
				login
			}
		} = this.props;

		login(values);
	}

	render() {
		const { handleSubmit } = this.props;

		return (
			<form onSubmit={handleSubmit(::this.onSubmit)}>
				<Field
					component={Input}
					validate={[required, email]}
					label="Email"
					name="email"
					className="input"
				/>
				<Field
					component={Input}
					validate={[required]}
					label="Password"
					name="password"
					className="input"
					type="password"
				/>
				<button type="submit">Sign in</button>
			</form>
		);
	}
}

export default LoginForm;
