import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { func, object, oneOfType } from 'prop-types';

import Input from '../../../common/components/form/input.jsx';

import { required, samePassword } from '../../../helpers/validation.helper.js';

@reduxForm({
	form: 'sign',
	enableReinitialize: true
})
class Invitation extends React.Component {
	static propTypes = {
		handleSubmit: func.isRequired,
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired
	}

	onSubmit(values) {
		const {
			actions: {
				activate
			},
			match: {
				params: {
					token
				}
			}
		} = this.props;
		activate(token, values);
	}

	render() {
		const {
			handleSubmit
		} = this.props;

		return (
			<form onSubmit={handleSubmit(::this.onSubmit)}>
				<div className="margin--b-30">
					<Field
						component={Input}
						validate={[required]}
						label="First and Last name"
						name="name"
						className="input"
						autoComplete="no-name"
						placeholder="First and Last name"
					/>
				</div>
				<div className="margin--b-30">
					<Field
						component={Input}
						name="password"
						validate={[required]}
						className="input"
						type="password"
						placeholder="Password"
					/>
				</div>
				<div className="form-group">
					<Field
						component={Input}
						name="passwordRepeat"
						validate={[required, samePassword]}
						className="input"
						type="password"
						placeholder="Repeat password"
					/>
				</div>
				<button type="submit">Join</button>
			</form>
		);
	}
}

export default Invitation;
