import Dashboard from '../company/dashboard/containers/dashboard.cv.jsx';
import Users from '../common/containers/users.cv.jsx';
import Modules from '../common/containers/modules.cv.jsx';
import Lessons from '../common/containers/lessons.cv.jsx';
import Course from '../common/containers/course.cv.jsx';

export const routes = [
	{
		path: '/',
		component: Dashboard
	},
	{
		path: '/course/:id?',
		component: Course
	},
	{
		path: '/course/:cid/module/:id?',
		component: Modules
	},
	{
		path: '/course/:cid/module/:mid/lesson/:id?',
		component: Lessons
	},
	{
		path: '/users',
		component: Users
	}
];
