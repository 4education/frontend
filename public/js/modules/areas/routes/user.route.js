import Dashboard from '../users/dashboard/containers/dashboard.cv.jsx';
import Modules from '../common/containers/modules.cv.jsx';
import Lessons from '../common/containers/lessons.cv.jsx';

export const routes = [
	{
		path: '/',
		component: Dashboard
	},
	{
		path: '/course/:id',
		component: Modules
	},
	{
		path: '/course/:cid/module/:mid',
		component: Lessons
	}
];
