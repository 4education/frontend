import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as courses from '../../../common/actions/content/courses.action.js';
import EventHandling from '../../../../../libraries/warnings/index.jsx';

// Components
import Header from '../../../../../common/components/includes/header.manage.jsx';
import Courses from "../../../common/components/content/Courses.jsx";

const mapStateToProps = (state) => {
	return {
		...state.dashboard,
		...state.content,
		oauth: state.oauth,
		realTimeForm: state.form
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...courses
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class Dashboard extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		warnings: oneOfType([object]),
		oauth: oneOfType([object]),
		courses: oneOfType([object])
	}

	static defaultProps = {
		warnings: null,
		oauth: {},
		courses: {}
	}


	componentDidMount() {
		const {
			actions: {
				getMyCourseList
			}
		} = this.props;

		getMyCourseList();
	}

	render() {
		const {
			warnings,
			oauth: {
				user: {
					role
				}
			}
		} = this.props;
		console.log(this.props);

		return (
			<React.Fragment>
				<Header />
				<EventHandling warnings={warnings} />
				<p>{role}</p>
				<Courses {...this.props} />
			</React.Fragment>
		);
	}
}

export default Dashboard;
