import {
	GET_USERS,
	GET_GROUPS,
	INVITE_USER,
	CREATE_NEW_GROUP,
	REMOVE_GROUP,
	UPDATE_GROUP
} from '../constants/users.const.js';

const initialState = {};

export default function users(state = initialState, action) {
	switch (action.type) {
		case GET_USERS:
			return {
				...state,
				users: action.users
			};
		case GET_GROUPS:
			return {
				...state,
				groups: action.groups
			};
		case REMOVE_GROUP:
			return {
				...state,
				groups: {
					items: state.groups.items.filter((item) => {
						if (item.gid !== action.gid) {
							return item;
						}
					})
				}
			};
		case UPDATE_GROUP:
			const { gid } = action.group,
				  { groups: { items: itemsForUpdate } } = state;

			return {
				...state,
				groups: {
					...state.groups,
					items: itemsForUpdate.map((item) => {
						if (item.gid === gid) {
							return action.group;
						}
						return item;
					})
				}
			};
		case CREATE_NEW_GROUP:
			return {
				...state,
				groups: {
					items: [
						...state.groups.items,
						action.group
					]
				}
			};
		case INVITE_USER:
			const { email } = action.email;
			return {
				...state,
				invited: [
					...state.invited,
					email
				]
			};
		default:
			return state;
	}
}
