import {
	GET_COURSES,
	GET_MODULES,
	GET_LESSONS,
	SET_COURSE,
	SET_MODULES,
	REMOVE_MODULES,
	REMOVE_COURSES,
	SET_LESSON,
	REMOVE_LESSON
} from '../constants/content.const.js';
import {GET_COURSE_USERS} from "../constants/content.const";

const initialState = {};

export default function content(state = initialState, action) {
	switch (action.type) {
		case GET_COURSES:
			return {
				...state,
				courses: action.corses
			};
		case SET_COURSE:
			return {
				...state,
				course: action.course
			};
		case SET_LESSON:
			return {
				...state,
				lesson: action.lesson
			};
		case GET_LESSONS:
			return {
				...state,
				lessons: action.lessons
			};
		case GET_MODULES:
			return {
				...state,
				modules: action.modules
			};
		case SET_MODULES:
			return {
				...state,
				module: action.module
			};
		case REMOVE_MODULES:
			const { courses: { items: removedModuleItems } } = state;
			return {
				...state,
				modules: {
					items: removedModuleItems.filter((item) => {
						if (item.id !== action.id) {
							return item;
						}
					})
				}
			};
		case REMOVE_LESSON:
			const { courses: { items: removedLessonItems } } = state;
			return {
				...state,
				modules: {
					items: removedLessonItems.filter((item) => {
						if (item.id !== action.id) {
							return item;
						}
					})
				}
			};
		case REMOVE_COURSES:
			const { courses: { items: removedCourseItems } } = state;
			return {
				...state,
				courses: {
					...state.courses,
					items: removedCourseItems.filter((item) => {
						if (item.id !== action.id) {
							return item;
						}
					})
				}
			};
		case GET_COURSE_USERS:
			return {
				...state,
				users: action.users
			};
		default:
			return state;
	}
}
