export const GET_USERS = 'GET_USERS';
export const GET_GROUPS = 'GET_GROUPS';
export const INVITE_USER = 'INVITE_USER';
export const CREATE_NEW_GROUP = 'CREATE_NEW_GROUP';
export const REMOVE_GROUP = 'REMOVE_GROUP';
export const UPDATE_GROUP = 'UPDATE_GROUP';
