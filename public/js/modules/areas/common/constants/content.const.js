export const GET_COURSES = 'GET_COURSES';
export const GET_MODULES = 'GET_MODULES';
export const GET_LESSONS = 'GET_LESSONS';

export const SET_COURSE = 'SET_COURSE';
export const SET_MODULES = 'SET_MODULES';
export const SET_LESSON = 'SET_LESSON';
export const SET_MATERIAL = 'SET_MATERIAL';

export const REMOVE_MODULES = 'REMOVE_MODULES';
export const REMOVE_COURSES = 'REMOVE_COURSES';
export const REMOVE_LESSON = 'REMOVE_LESSON';

export const GET_COURSE_USERS = 'GET_COURSE_USERS';
