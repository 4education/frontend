import React from 'react';
import {
	func
} from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import '../../../../../styl/blocks/modal.styl';

// Helpers
import { required, email } from '../../../../helpers/validation.helper.js';

// Components
import Modal from '../../../../common/components/includes/modal.jsx';
import Input from '../../../../common/components/form/input.jsx';
import Select from '../../../../common/components/form/select.jsx';
import * as courses from "../actions/content/courses.action";

const mapStateToProps = (state) => {
		return {
			...state.content
		};
	},

	mapDispatchToProps = (dispatch) => {
		return {
			actions: bindActionCreators({
				...courses
			}, dispatch),
			dispatch
		};
	};

@connect(mapStateToProps, mapDispatchToProps)
@reduxForm({
	form: 'invitation',
	enableReinitialize: true
})
class ModalUserAssign extends React.Component {
	static propTypes = {
		onClose: func,
		handleSubmit: func.isRequired,
	}

	constructor(props) {
		super(props);
		this.state = {
			...this.state,
			listType: 'add'
		};
	}

	static defaultProps = {
		onClose: null,
		users: {},
		cid: null
	}

	addOrRemoveUser(uid, courseId, type)
	{
		console.log(uid, courseId);
		const {
			actions: {
				addCourseToUser,
				removeCourseToUser
			}
		} = this.props;
		if(type === 'add')
		{
			return addCourseToUser(uid, courseId);
		}
		return removeCourseToUser(uid, courseId);
	}

	switchLists()
	{
		const {
			actions: {
				getNotInCourse,
				getInCourse
			},
			cid
		} = this.props;
		if(this.state.listType === 'add')
		{
			getInCourse(cid);
		}
		else
		{
			getNotInCourse(cid);
		}

		this.setState(prevState => ({
			...prevState,
			listType: prevState.listType === 'add' ? 'remove' : 'add'
		}));
	}

	componentDidMount() {
		const {
			actions: {
				getNotInCourse
			},
			cid
		} = this.props;

		if (cid) {
			getNotInCourse(cid);
		}
	}

    render() {
		const {
			onClose,
			handleSubmit,
			users: {
				items
			},
			cid,
		} = this.props;

		const {
			listType
		} = this.state;
		console.log(this.props);

        return (
			<Modal onCancel={onClose}>
				<button type="button" onClick={() => { this.switchLists(); }}>{
					(listType === 'add' ?  "Switch to Remove" : "Switch to Add") || null
				}</button>
				<ul>
					{
						items && items.length && items.map((item) => {
							const { uid, name, email } = item;
							return (
								<li className="text" key={uid}>{name} ({email})
									<button type="button" onClick={() => { this.addOrRemoveUser(uid, cid, listType); }}>{
										(listType === 'add' ?  "Add" : "Remove") || null
									}</button>
								</li>
							);
						})
					}
				</ul>
			</Modal>
        );
    }
}

export default ModalUserAssign;
