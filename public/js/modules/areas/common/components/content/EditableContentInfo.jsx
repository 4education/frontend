import React from 'react';
import {
	func,
	oneOfType,
	object,
	bool
} from 'prop-types';
import { Field, reduxForm } from 'redux-form';

// Components
import Input from '../../../../../common/components/form/input.jsx';
import Textarea from '../../../../../common/components/form/textarea.jsx';
import Upload from '../../../../../common/components/form/upload.jsx';

// Helpers
import { required } from '../../../../../helpers/validation.helper.js';

@reduxForm({ enableReinitialize: true, forceUnregisterOnUnmount: true })
class EditableContentInfo extends React.Component {
	static propTypes = {
		handleSubmit: func.isRequired,
		content: oneOfType([object]),
		image: bool
	}

	static defaultProps = {
		content: {},
		image: true
	}

	__onAssignUsers = () => {

	}

	__onAssignToGroup = () => {

	}

    render() {
		const {
			handleSubmit,
			content: {
				id
			},
			image
		} = this.props;

        return (
			<React.Fragment>
				<form onSubmit={handleSubmit}>
					{
						(image && (
							<Field
								component={Upload}
								name="titleImageUrl"
							/>
						)) || null
					}
					<Field
						component={Input}
						validate={[required]}
						label="Title"
						name="name"
						className="input"
						placeholder="Title"
					/>
					<Field
						component={Textarea}
						validate={[required]}
						label="Description"
						name="description"
						className="input"
						placeholder="Short description"
					/>
					{
						(id && (
							<Field
								component={Input}
								validate={[required]}
								name="id"
								type="hidden"
							/>
						)) || null
					}
					<button type="submit">Save</button>
				</form>
				{
					id ? (
						<React.Fragment>
							<button type="button" onClick={this.__onAssignUsers}> Assing user </button>
							<button type="button" onClick={this.__onAssignToGroup}> Assing user </button>
						</React.Fragment>
					) : null
				}
			</React.Fragment>
        );
    }
}
export default EditableContentInfo;
