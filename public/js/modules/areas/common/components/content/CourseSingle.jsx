import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType
} from 'prop-types';
import Pagination from '../../../../../common/components/pagination/pagination-ant.jsx';

// Helpers
import { normalizeOffset } from '../../../../../helpers/filters/limit.helper.js';

// Configuration
import { limit } from '../../../../../configuration/rules.json';
import EditableContentInfo from './EditableContentInfo.jsx';

// Components
import SingleBlock from './SingleBlock.jsx';


class CourseSingle extends React.Component {
	static propTypes = {
		modules: oneOfType([object]),
		course: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		modules: {},
		course: {}
	}

	__onUpdateUpsertCreate = (values) => {
		const {
			actions: {
				addOrEditCourse
			}
		} = this.props;

		addOrEditCourse(values);
	}

	__onRemove = (id) => {
		const {
			actions: {
				removeSingleModule
			}
		} = this.props;

		if (!confirm('Are you sure?')) return;

		removeSingleModule(id);
	}

    render() {
		const {
			course,
			course: {
				id: cid
			},
			modules: {
				filters,
				items = [],
				totalCount
			}
		} = this.props,
		pageActive = normalizeOffset(filters);

        return (
			<React.Fragment>
				<EditableContentInfo
					form="course"
					initialValues={{ ...course }}
					content={course}
					onSubmit={this.__onUpdateUpsertCreate}
					{...this.props}
				/>
				{
					cid && (
						<section>
							<h2> Module List </h2>
							<ul className="fl">
								{
									(items.length && items.map((item) => {
										const {
											id
										} = item;
										return (
											<SingleBlock
												total="lessonsCount"
												key={id}
												item={item}
												link={`/course/${cid}/module/${id}`}
												onRemove={this.__onRemove}
											/>
										);
									})) || null
								}
								<li>
									<Link type="button" to={`/course/${cid}/module`}>
										Add new module
									</Link>
								</li>
							</ul>
						</section>
					)
				}
				{(items.length && ((totalCount / limit) > 1) &&
					(
						<div className="pagination fl fl--justify-end">
							<Pagination
								hideOnSinglePage
								total={totalCount}
								pageSize={limit}
								current={(pageActive ? pageActive + 1 : 1)}
								onChange={this.__onPageChange}
							/>
						</div>
					)) || null}
			</React.Fragment>
        );
    }
}
export default CourseSingle;
