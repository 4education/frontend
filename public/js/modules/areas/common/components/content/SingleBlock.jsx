import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType,
	bool,
	string,
	func
} from 'prop-types';


// Configuration
import configuration from '../../../../../configuration/host.js';
import ModalUserAssign from '../ModalUserAssign.jsx';

const {
	protocol,
	fileStorageService
} = configuration;

class SingleBlock extends React.Component {
	static propTypes = {
		item: oneOfType([object]).isRequired,
		preview: bool,
		link: string.isRequired,
		total: string,
		onRemove: func
	}

	static defaultProps = {
		preview: false,
		onRemove: null,
		total: ''
	}

	constructor(props) {
		super(props);
		this.state = {
			modalUser: false,
			modalGroup: false,
			src: null
		};
	}
	__onClick = () => {
		this.setState(prevState => ({
			...prevState,
			modalUser: !prevState.modalUser
		}));
	}

	__onShow = (type, e) => {
		e.preventDefault();
		console.log(type);
		console.log(this.state.modalUser);
		switch (type) {
			case "modalUser":
				return this.setState(prevState => ({
					...prevState,
					modalUser: !prevState.modalUser
				}));
			case "modalGroup":
				return this.setState(prevState => ({
					...prevState,
					modalGroup: !prevState.modalGroup
				}));
		}
	}

	componentDidMount() {
		const {
			item: {
				titleImageUrl
			}
		} = this.props;
		this.__onLoadImages(titleImageUrl);
	}

	componentDidUpdate(prevProps) {
		const {
			item: {
				titleImageUrl: prevImage
			}
		} = prevProps,
		{
			item: {
				titleImageUrl: currentImage
			}
		} = this.props;

		if (prevImage !== currentImage) {
			this.__onLoadImages(currentImage);
		}
	}

	__onLoadImages(value) {
		if (!value) return;

		const img = new Image(),
			  url = `${protocol}${fileStorageService}/${value}`;

		img.src = url;

        img.onload = () => {
            this.setState(prevState => ({
				...prevState,
				src: url
			}));
        };
        img.onerror = () => {
			this.setState(prevState => ({
				...prevState,
				src: '/img/missing.png'
			}));
        };
    }

    render() {
		const {
			total,
			item: {
				id,
				description,
				name,
				[total]: count
			},
			preview,
			link,
			onRemove
		} = this.props,
		{
			src,
			modalUser,
			modalGroup
		} = this.state;

        return (
			<div>
				<Link
					key={id}
					style={{
						border: '1px solid red',
						margin: '10px',
						padding: '20px',
						display: 'block'
					}}
					to={link}
				>
					{
						(src && <img width="150" alt={name} src={src} />) || null
					}
					<p>{name}</p>
					<p>{description}</p>
					{
						(!preview && (
							<React.Fragment>
								{ (count && <p>Total: {count}</p>) || null }
								<button
									type="button"
									onClick={this.__onShow.bind(this, 'modalUser')}
								>
									Assign to users
								</button>
								<button
									type="button"
									onClick={this.__onShow.bind(this, 'modalGroup')}
								>
									Assign to groups
								</button>
							</React.Fragment>
						)) || null
					}
				</Link>
				<button type="button" onClick={() => { onRemove(id); }}>Remove</button>
				{
					(modalUser && (
						<ModalUserAssign
							onClose={this.__onClick}
							onSubmit={this.__onSubmit}
							cid={id}
						/>
					)) || null
				}
			</div>
        );
    }
}
export default SingleBlock;
