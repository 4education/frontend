import React from 'react';
import {
	func, oneOfType, object
} from 'prop-types';
import Modal from '../../../../../common/components/includes/modal.jsx';

import Text from '../materials/Text.jsx';

class ModalMaterialWithTypes extends React.Component {
	static propTypes = {
		onChoose: func,
		onClose: func,
		single: oneOfType([object]),
		lesson: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		onChoose: null,
		onClose: null,
		single: {},
		lesson: {}
	}

	constructor(props) {
		super(props);
		const { single } = props;
		this.state = {
			fullList: true,
			single
		};
	}

	onSingleRerender(type) {
		const { single } = this.state;
		switch (type) {
			case 'text':
				return (
					<Text
						initialValues={{ ...single }}
						onSubmit={this.__onSubmitBlock}
					/>
				);
			default:
				break;
		}
		return null;
	}

	__onSubmitBlock = (values) => {
		const {
			actions: {
				addOrEditMaterials
			},
			lesson: {
				id: lid
			}
		} = this.props;

		addOrEditMaterials(values, lid);
	}

	__onFullList = () => {
		this.setState(prevState => ({
			...prevState,
			fullList: true,
			single: {}
		}));
	}

	__onChangeView = (type) => {
		this.setState(prevState => ({
			...prevState,
			fullList: false,
			single: {
				type
			}
		}));
	}

    render() {
		const {
			onClose
		} = this.props,
		{
			fullList,
			single: {
				type
			}
		} = this.state;

        return (
			<Modal onCancel={onClose}>
				{
					(fullList && (
						<div>
							<p>Simple Text</p>
							<button type="button" onClick={() => { this.__onChangeView('text'); }}>Add text block</button>
						</div>
					)) || null
				}
				{
					(type && (
						<section>
							<button type="button" onClick={this.__onFullList}>Back to list</button>
							{ this.onSingleRerender(type) }
						</section>
					)) || null
				}
			</Modal>
        );
    }
}
export default ModalMaterialWithTypes;
