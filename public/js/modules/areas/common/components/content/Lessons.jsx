import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType
} from 'prop-types';

// Components
import EditableContentInfo from './EditableContentInfo.jsx';
import Material from '../materials/Material.jsx';
import ModalMaterialWithTypes from './ModalMaterialWithTypes.jsx';

class Lessons extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		lesson: oneOfType([object]),
		module: oneOfType([object]),
		course: oneOfType([object])
	}

	static defaultProps = {
		lesson: {},
		module: {},
		course: {}
	}

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
	}

	componentDidUpdate(prevProps) {
		const { lesson: prevLesson } = prevProps,
			  { lesson: currentLesson } = this.props;

		if (Object.keys(prevLesson).length && !_.isEqual(prevLesson, currentLesson)) {
			this.__onModal();
		}
	}

	__onUpdateUpsertCreate = (values) => {
		const {
			actions: {
				addOrEditLesson
			},
			course: {
				id: cid
			},
			module: {
				id: mid
			}
		} = this.props;

		addOrEditLesson({
			...values,
			courseId: cid,
			moduleId: mid
		});
	}

	__onModal = () => {
		this.setState(prevState => ({
			...prevState,
			modal: !prevState.modal
		}));
	}

    render() {
		const {
			lesson,
			lesson: {
				id,
				materialItems
			}
		} = this.props,
		{ modal } = this.state;

        return (
			<section>
				<h2>Lesson information</h2>
				<EditableContentInfo
					form="lesson"
					initialValues={{ ...lesson }}
					content={lesson}
					image={false}
					onSubmit={this.__onUpdateUpsertCreate}
					{...this.props}
				/>
				{
					(id && (
						<div className="fl fl--dir-col">
							{
								(materialItems && materialItems.length && materialItems.map((item) => {
									const { sid } = item;
									return (
										<Material key={sid} {...item} />
									);
								})) || null
							}
							<button onClick={this.__onModal} type="button">Append new block</button>
						</div>
					)) || null
				}
				{
					(modal && (
						<ModalMaterialWithTypes
							onClose={this.__onModal}
							onChoose={this.__onChoose}
							{...this.props}
						/>
					)) || null
				}
			</section>
        );
    }
}

export default Lessons;
