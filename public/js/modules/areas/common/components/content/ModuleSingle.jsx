import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType
} from 'prop-types';
import Pagination from '../../../../../common/components/pagination/pagination-ant.jsx';

// Helpers
import { normalizeOffset } from '../../../../../helpers/filters/limit.helper.js';

// Configuration
import { limit } from '../../../../../configuration/rules.json';
import EditableContentInfo from './EditableContentInfo.jsx';

// Components
import SingleBlock from './SingleBlock.jsx';


class ModuleSingle extends React.Component {
	static propTypes = {
		lessons: oneOfType([object]),
		course: oneOfType([object]),
		module: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		lessons: {},
		course: {},
		module: {}
	}

	__onUpdateUpsertCreate = (values) => {
		const {
			actions: {
				addOrEditModule
			},
			course: {
				id: cid
			}
		} = this.props;

		addOrEditModule({
			...values,
			courseId: cid
		});
	}

	__onRemove = (id) => {
		const {
			actions: {
				removeSingleLesson
			}
		} = this.props;

		if (!confirm('Are you sure?')) return;

		removeSingleLesson(id);
	}

    render() {
		const {
			course: {
				id: cid
			},
			module,
			module: {
				id: mid
			},
			lessons: {
				filters,
				items = [],
				totalCount
			}
		} = this.props,
		pageActive = normalizeOffset(filters);

        return (
			<React.Fragment>
				<section>
					<h2> This content you can edit </h2>
					<EditableContentInfo
						form="module"
						initialValues={{ ...module }}
						content={module}
						onSubmit={this.__onUpdateUpsertCreate}
						{...this.props}
					/>
				</section>
				<section style={{ width: '100%' }}>
					{
						mid && (
							<section>
								<h2> Lessons list </h2>
								<ul className="fl">
									{
										(items.length && items.map((item) => {
											const {
												id
											} = item;
											return (
												<SingleBlock
													key={id}
													item={item}
													onRemove={this.__onRemove}
													link={`/course/${cid}/module/${mid}/lesson/${id}`}
												/>
											);
										})) || null
									}
									<li>
										<Link type="button" to={`/course/${cid}/module/${mid}/lesson`}>
											Add new lesson
										</Link>
									</li>
								</ul>
							</section>
						)
					}
					{(items.length && ((totalCount / limit) > 1) &&
						(
							<div className="pagination fl fl--justify-end">
								<Pagination
									hideOnSinglePage
									total={totalCount}
									pageSize={limit}
									current={(pageActive ? pageActive + 1 : 1)}
									onChange={this.__onPageChange}
								/>
							</div>
						)) || null}
				</section>
			</React.Fragment>
        );
    }
}
export default ModuleSingle;
