import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType
} from 'prop-types';

// Helpers
import { normalizeOffset } from '../../../../../helpers/filters/limit.helper.js';

// Configuration
import { limit } from '../../../../../configuration/rules.json';

// Components
import Pagination from '../../../../../common/components/pagination/pagination-ant.jsx';
import SingleBlock from './SingleBlock.jsx';

class Courses extends React.Component {
	static propTypes = {
		courses: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		courses: {}
	}

	__onRemove = (id) => {
		const {
			actions: {
				removeSingleCourse
			}
		} = this.props;

		if (!confirm('Are you sure?')) return;

		removeSingleCourse(id);
	}

    render() {
		const {
			courses: {
				filters,
				items = [],
				totalCount
			}
		} = this.props,
		pageActive = normalizeOffset(filters);

        return (
			<React.Fragment>
				<section>Courses</section>
				<Link to="/course">Add new course</Link>
				<div>
					{
						(items.length && items.map((item) => {
							const { id } = item;
							return (
								<SingleBlock
									total="modulesCount"
									link={`/course/${id}`}
									key={id}
									item={item}
									onRemove={this.__onRemove}
								/>
							);
						})) || null
					}
				</div>
				{(items.length && ((totalCount / limit) > 1) &&
					(
						<div className="pagination fl fl--justify-end">
							<Pagination
								hideOnSinglePage
								total={totalCount}
								pageSize={limit}
								current={(pageActive ? pageActive + 1 : 1)}
								onChange={this.__onPageChange}
							/>
						</div>
					)) || null}
			</React.Fragment>
        );
    }
}
export default Courses;
