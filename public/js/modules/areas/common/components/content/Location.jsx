import React from 'react';
import { Link } from 'react-router-dom';
import {
	oneOfType,
	array
} from 'prop-types';

class Location extends React.Component {
	static createUrl(item, type) {
		let link = '';
		switch (type) {
			case 'course':
				const { id: cid } = item;
				link = `/course/${cid}`;
				break;
			case 'module':
				const { id: mid, cid: courseID } = item;
				link = `/course/${courseID}/module/${mid}?`;
				break;
			default:
				break;
		}
		return link;
	}

	static propTypes = {
		data: oneOfType([array]).isRequired
	}

	static defaultProps = {}

    render() {
		const {
			data
		} = this.props;

        return (
			<React.Fragment>
				{
					data.length && (
						<div className="fl">
							<p className="text">You locate here &gt;</p>
							<ul className="fl">
								{
									data.map((item) => {
										const { name, id, type } = item;
										return (
											<li key={id} style={{ padding: '5px 20px' }}>
												<Link to={Location.createUrl(item, type)}>{name}</Link>
											</li>
										);
									})
								}
							</ul>
						</div>
					)
				}
			</React.Fragment>
        );
    }
}
export default Location;
