import React from 'react';
import {
	func,
	oneOfType,
	object
} from 'prop-types';
import { Field, reduxForm } from 'redux-form';

import '../../../../../styl/blocks/modal.styl';

// Helpers
import { required } from '../../../../helpers/validation.helper.js';

// Components
import Modal from '../../../../common/components/includes/modal.jsx';
import Input from '../../../../common/components/form/input.jsx';

@reduxForm({
	form: 'groups',
	enableReinitialize: true
})
class ModalGroupCreate extends React.Component {
	static propTypes = {
		onClose: func,
		handleSubmit: func.isRequired,
		edited: oneOfType([object])
	}

	static defaultProps = {
		onClose: null,
		edited: {}
	}

    render() {
		const {
			onClose,
			handleSubmit,
			edited: { gid }
		} = this.props;

        return (
			<Modal onCancel={onClose}>
				<form onSubmit={handleSubmit}>
					<div className="margin--b-30">
						<Field
							component={Input}
							validate={[required]}
							label="Group name"
							name="name"
							className="input"
						/>
					</div>
					{
						(gid && (
							<Field
								component={Input}
								name="gid"
								type="hidden"
							/>
						)) || null
					}
					<button type="submit">Save</button>
				</form>
			</Modal>
        );
    }
}

export default ModalGroupCreate;
