import React from 'react';
import { Link } from 'react-router-dom';
import {
	object,
	oneOfType,
	string
} from 'prop-types';
import DOMPurify from 'dompurify';

class Materials extends React.Component {
	static propTypes = {
		type: string,
		attributes: oneOfType([object])
	}

	static defaultProps = {
		type: 'text',
		attributes: {}
	}

	renderByType(type) {
		switch (type) {
			case 'text':
				const { attributes: { text } } = this.props;
				return (
					<div dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(text) }} />
				);
			default:
				return null;
		}
	}

    render() {
		const { type } = this.props;
        return (
			<React.Fragment>
				<div>
					{ this.renderByType(type) }
				</div>
			</React.Fragment>
        );
    }
}
export default Materials;
