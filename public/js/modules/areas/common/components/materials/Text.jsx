import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
	func, string
} from 'prop-types';
import TextArea from '../../../../../common/components/form/textarea.jsx';
import Input from '../../../../../common/components/form/input.jsx';

import { required } from '../../../../../helpers/validation.helper.js';

@reduxForm({
	form: 'text',
	enableReinitialize: true
})
class Text extends React.Component {
	static propTypes = {
		handleSubmit: func.isRequired,
		sid: string
	}

	static defaultProps = {
		sid: null
	}

    render() {
		const {
			handleSubmit,
			sid
		} = this.props;
        return (
			<div>
				<p>Put your text below</p>
				<form onSubmit={handleSubmit}>
					<Field
						component={TextArea}
						validate={[required]}
						label="Comany name"
						name="attributes.text"
						className="input"
						autoComplete="no-company"
					/>
					{
						(sid && (
							<Field
								component={Input}
								type="hidden"
							/>
						)) || null
					}
					<button type="submit">Add</button>
				</form>
			</div>
        );
    }
}
export default Text;
