import React from 'react';
import {
	oneOfType,
	object
} from 'prop-types';

// Components
import ModalGroupCreate from './ModalGroupCreate.jsx';

class GroupsComponent extends React.Component {
	static propTypes = {
		groups: oneOfType([object]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		groups: {}
	}

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
	}

	componentDidUpdate(prevProps) {
		const { groups: prevGroups } = prevProps,
			  { groups: currentGroups } = this.props,
			  { modal } = this.state;
		if (!_.isEqual(prevGroups, currentGroups) && modal) {
			this.__onClick();
		}
	}

	__onSort = (gid) => {
		console.log(gid)
	}

	__onUpdateUpsertCreate = (values) => {
		const {
			actions: {
				addGroup,
				updateGroup
			}
		} = this.props,
		{ gid } = values;

		if (gid) {
			updateGroup(values);
		} else {
			addGroup(values);
		}
	}	

	__onRemove = (gid) => {
		const {
			actions: {
				removeGroup
			}
		} = this.props;

		if (!confirm('Are you sure?')) return;

		removeGroup(gid);
	}

	__onEdit = (gid, name) => {
		this.setState(prevState => ({
			...prevState,
			modal: true,
			edited: {
				gid,
				name
			}
		}));
	}

	__onClick = () => {
		this.setState(prevState => ({
			...prevState,
			modal: !prevState.modal,
			edited: {}
		}));
	}

    render() {
		const {
			groups: {
				items
			}
		} = this.props,
		{
			modal,
			edited
		} = this.state;

        return (
			<section>
				<div>
					<p>Group List</p>
					{
						(items && items.length && (
							<ul>
								{
									items.map((item) => {
										const { gid, name } = item;
										return (
											<li key={gid} role="presentation" onClick={() => this.__onSort(gid)}>
												{name}
												<button
													type="button"
													onClick={(event) => {
														event.stopPropagation();
														this.__onEdit(gid, name);
													}}
												>
													Edit
												</button>
												<button
													type="button"
													onClick={(event) => {
														event.stopPropagation();
														this.__onRemove(gid);
													}}
												>
													Remove
												</button>
											</li>
										);
									})
								}
							</ul>
						)) || null
					}
					<button type="button" onClick={this.__onClick}>Create new group</button>
				</div>
				{
					(modal && (
						<ModalGroupCreate
							initialValues={{ ...edited }}
							edited={edited}
							onClose={this.__onClick}
							onSubmit={this.__onUpdateUpsertCreate}
						/>
					)) || null
				}
			</section>
        );
    }
}

export default GroupsComponent;
