import React from 'react';
import {
	func,
	oneOfType,
	object,
	array
} from 'prop-types';

import '../../../../../styl/blocks/modal.styl';

// Components
import ModalUserInvitation from './ModalUserInvitation.jsx';

class UserListComponent extends React.Component {
	static propTypes = {
		oauth: oneOfType([object]),
		users: oneOfType([object]),
		invited: oneOfType([array]),
		actions: oneOfType([object]).isRequired
	}

	static defaultProps = {
		oauth: {},
		users: {},
		invited: []
	}

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
	}

	componentDidUpdate(prevProps) {
		const { invited: prevInvited } = prevProps,
			  { invited: currentInvited } = this.props;

		if (currentInvited && currentInvited.length > prevInvited.length) {
			this.__onClick();
		}
	}

	__onSubmit = (value) => {
		const {
			oauth: {
				user: {
					cid
				}
			},
			actions: {
				inviteUser
			}
		} = this.props;

		if (!cid) return;

		inviteUser({ cid, ...value });
	}

	__onClick = () => {
		this.setState(prevState => ({
			...prevState,
			modal: !prevState.modal
		}));
	}

    render() {
		const {
			users: {
				items
			}
		} = this.props,
		{
			modal
		} = this.state;

        return (
			<React.Fragment>
				<section>
					<p>User list</p>
					<button type="button" onClick={this.__onClick} className="button">Invite user</button>
					<div>
						<div>
							<p>Sort by:</p>
							<p>Search:</p>
						</div>
						<ul>
							{
								items && items.length && items.map((item) => {
									const { uid, name, email } = item;
									return (
										<li className="text" key={uid}>{name} ({email})</li>
									);
								})
							}
						</ul>
					</div>
				</section>
				{
					(modal && (
						<ModalUserInvitation
							onClose={this.__onClick}
							onSubmit={this.__onSubmit}
						/>
					)) || null
				}
			</React.Fragment>
        );
    }
}

export default UserListComponent;
