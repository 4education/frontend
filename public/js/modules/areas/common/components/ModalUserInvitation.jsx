import React from 'react';
import {
	func
} from 'prop-types';
import { Field, reduxForm } from 'redux-form';

import '../../../../../styl/blocks/modal.styl';

// Helpers
import { required, email } from '../../../../helpers/validation.helper.js';

// Components
import Modal from '../../../../common/components/includes/modal.jsx';
import Input from '../../../../common/components/form/input.jsx';
import Select from '../../../../common/components/form/select.jsx';

@reduxForm({
	form: 'invitation',
	initialValues: {
		url: 'customer'
	},
	enableReinitialize: true
})
class ModalUserInvitation extends React.Component {
	static propTypes = {
		onClose: func,
		handleSubmit: func.isRequired
	}

	static defaultProps = {
		onClose: null
	}

    render() {
		const {
			onClose,
			handleSubmit
		} = this.props;

        return (
			<Modal onCancel={onClose}>
				<form onSubmit={handleSubmit}>
					<div className="margin--b-30">
						<Field
							component={Select}
							validate={[required]}
							label="Type of user"
							name="url"
							options={[
								{ key: 'employee', name: 'Employee', label: 'Employee' },
								{ key: 'customer', name: 'Customer', label: 'Customer' }
							]}
							className="select"
							placeholder="Type of user"
						/>
					</div>
					<div className="margin--b-30">
						<Field
							component={Input}
							validate={[required, email]}
							label="Email"
							name="email"
							className="input"
						/>
					</div>
					<button type="submit">Invite</button>
				</form>
			</Modal>
        );
    }
}

export default ModalUserInvitation;
