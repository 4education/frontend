import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as lessons from '../actions/content/lessons.action.js';
import * as modules from '../actions/content/modules.action.js';
import * as courses from '../actions/content/courses.action.js';
import * as materials from '../actions/content/materials.action.js';
import EventHandling from '../../../../libraries/warnings/index.jsx';

// Components
import Header from '../../../../common/components/includes/header.manage.jsx';
import LessonsComponent from '../components/content/Lessons.jsx';
import Location from '../components/content/Location.jsx';

const mapStateToProps = (state) => {
	return {
		...state.content,
		oauth: state.oauth,
		realTimeForm: state.form
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...lessons,
			...modules,
			...courses,
			...materials
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class Lessons extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired,
		warnings: oneOfType([object]),
		oauth: oneOfType([object]),
		module: oneOfType([object]),
		course: oneOfType([object])
	}

	static defaultProps = {
		warnings: null,
		oauth: {},
		module: {},
		course: {}
	}

	componentDidMount() {
		const {
			actions: {
				getSingleModule,
				getSingleCourse,
				getSingleLesson
			},
			match: {
				params: {
					id: lid,
					cid,
					mid
				}
			},
			module: {
				id: currentMid
			},
			course: {
				id: currentCid
			}
		} = this.props;

		if (cid && currentCid !== cid) {
			getSingleCourse(cid);
		}

		if (mid && currentMid !== mid) {
			getSingleModule(mid);
		}

		if (lid) {
			getSingleLesson(lid);
		}
	}

	render() {
		const {
			warnings,
			course,
			module
		} = this.props;

		return (
			<React.Fragment>
				<Header />
				<EventHandling warnings={warnings} />
				{
					(
						Object.keys(course).length &&
						Object.keys(module).length &&
						(
							<Location data={[{ type: 'course', ...course }, { type: 'module', ...module, cid: course.id }]} />
						)
					) || null
				}
				<LessonsComponent {...this.props} />
			</React.Fragment>
		);
	}
}

export default Lessons;
