import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as modules from '../actions/content/modules.action.js';
import * as courses from '../actions/content/courses.action.js';
import * as lessons from '../actions/content/lessons.action.js';
import EventHandling from '../../../../libraries/warnings/index.jsx';

// Helpers
import { mergeOffsetWithFilters } from '../../../../helpers/filters/limit.helper.js';


// Components
import Header from '../../../../common/components/includes/header.manage.jsx';
import ModuleSingle from '../components/content/ModuleSingle.jsx';
import SingleBlock from '../components/content/SingleBlock.jsx';

const mapStateToProps = (state) => {
	return {
		...state.content,
		oauth: state.oauth,
		realTimeForm: state.form
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...modules,
			...courses,
			...lessons
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class Modules extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired,
		warnings: oneOfType([object]),
		oauth: oneOfType([object]),
		modules: oneOfType([object]),
		course: oneOfType([object])
	}

	static defaultProps = {
		warnings: null,
		oauth: {},
		modules: {},
		course: {}
	}

	componentDidMount() {
		const {
			actions: {
				getLessonsByModule,
				getSingleModule,
				getSingleCourse
			},
			match: {
				params: {
					id: mid,
					cid
				}
			},
			modules: {
				filters
			},
			course: {
				id: currentCid
			}
		} = this.props;

		if (cid && currentCid !== cid) {
			getSingleCourse(cid);
		}

		if (mid && cid) {
			getLessonsByModule(filters, cid, mid);
		}

		if (mid) {
			getSingleModule(mid);
		}
	}

	__onPageByCoursesChange = (page) => {
		const { actions: { getModuleByCourse }, modules: { filters } } = this.props,
				withOffset = mergeOffsetWithFilters(filters, (page ? page - 1 : 0));

		getModuleByCourse(withOffset);
	}

	render() {
		const {
			warnings,
			course,
			course: {
				id: cid
			}
		} = this.props;

		return (
			<React.Fragment>
				<Header />
				<EventHandling warnings={warnings} />
				<div className="fl fl--wrap">
					{
						(course && Object.keys(course).length && (
							<section>
								<h2> The preview of course that you want to add some modules </h2>
								<SingleBlock link={`/course/${cid}`} item={course} preview />
							</section>
						)) || null
					}
					<ModuleSingle {...this.props} />
				</div>
			</React.Fragment>
		);
	}
}

export default Modules;
