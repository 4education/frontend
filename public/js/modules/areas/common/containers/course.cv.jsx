import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as modules from '../actions/content/modules.action.js';
import * as courses from '../actions/content/courses.action.js';
import EventHandling from '../../../../libraries/warnings/index.jsx';

// Helpers
import { mergeOffsetWithFilters } from '../../../../helpers/filters/limit.helper.js';


// Components
import Header from '../../../../common/components/includes/header.manage.jsx';
import CourseSingle from '../components/content/CourseSingle.jsx';

const mapStateToProps = (state) => {
	return {
		...state.content,
		oauth: state.oauth,
		realTimeForm: state.form
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...modules,
			...courses
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class Course extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		match: oneOfType([object]).isRequired,
		warnings: oneOfType([object]),
		oauth: oneOfType([object]),
		course: oneOfType([object]),
		modules: oneOfType([object])
	}

	static defaultProps = {
		warnings: null,
		oauth: {},
		course: {},
		modules: {}
	}

	componentDidMount() {
		const {
			actions: {
				getModuleByCourse,
				getSingleCourse
			},
			match: {
				params: {
					id
				}
			},
			modules: {
				filters
			}
		} = this.props;

		if (id) {
			getModuleByCourse(filters, id);
			getSingleCourse(id);
		}
	}

	componentDidUpdate(prevProps) {
		const { course: prevCourse } = prevProps,
			  { course: currentCourse, actions: { getModuleByCourse } } = this.props;

		if (!_.isEqual(prevCourse, currentCourse) && currentCourse.cid) {
			getModuleByCourse(currentCourse.cid);
		}
	}

	__onPageByModulesChange = (page) => {
		const { actions: { getModuleByCourse }, modules: { filters } } = this.props,
				withOffset = mergeOffsetWithFilters(filters, (page ? page - 1 : 0));

		getModuleByCourse(withOffset);
	}

	render() {
		const {
			warnings
		} = this.props;

		return (
			<React.Fragment>
				<Header />
				<EventHandling warnings={warnings} />
				<CourseSingle
					onPageChnages={this.__onPageByModulesChange}
					{...this.props}
				/>
			</React.Fragment>
		);
	}
}

export default Course;
