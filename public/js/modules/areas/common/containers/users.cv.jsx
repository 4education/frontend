import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

import * as users from '../actions/users.action.js';
import EventHandling from '../../../../libraries/warnings/index.jsx';

// Components
import Header from '../../../../common/components/includes/header.manage.jsx';
import GroupsComponent from '../components/GroupsComponent.jsx';
import UserListComponent from '../components/UserListComponent.jsx';

const mapStateToProps = (state) => {
	return {
		...state.users,
		oauth: state.oauth,
		realTimeForm: state.form
	};
},

mapDispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators({
			...users
		}, dispatch),
		dispatch
	};
};

@connect(mapStateToProps, mapDispatchToProps)
class UserManagement extends React.Component {
	static propTypes = {
		actions: oneOfType([object]).isRequired,
		warnings: oneOfType([object])
	}

	static defaultProps = {
		warnings: null
	}

	componentDidMount() {
		const {
			actions: {
				getUserlList,
				getGroupList
			}
		} = this.props;

		getGroupList();
		getUserlList();
	}

	render() {
		const {
			warnings
		} = this.props;

		return (
			<React.Fragment>
				<Header />
				<EventHandling warnings={warnings} />
				<div className="fl fl--justify-b">
					<GroupsComponent {...this.props} />
					<UserListComponent {...this.props} />
				</div>
			</React.Fragment>
		);
	}
}

export default UserManagement;
