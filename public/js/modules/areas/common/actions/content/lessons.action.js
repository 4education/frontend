import { SET_WARNINGS } from '../../../../../libraries/warnings/constant/warnings.const.js';
import {
	GET_LESSONS,
	SET_LESSON,
	REMOVE_LESSON,
} from '../../constants/content.const.js';
import Api from '../../../../../service/api.js';

import { setDefaultLimit } from '../../../../../helpers/filters/limit.helper.js';

export function getLessonsByModule(filters, cid, mid) {
    return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				lessons = await Api.__request({
					url: `courses/${cid}/modules/${mid}/lessons${flt ? `?${flt}` : ''}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_LESSONS,
				lessons,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function removeSingleLesson(id) {
    return async (dispatch) => {
		try {
			await Api.__request({
					url: `lessons/${id}`,
					method: 'DELETE',
					type: 'content'
				});

			dispatch({
				type: REMOVE_LESSON,
				id
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function getSingleLesson(lid) {
    return async (dispatch) => {
		try {
			const lesson = await Api.__request({
					url: `lessons/${lid}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: SET_LESSON,
				lesson
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function addOrEditLesson(data) {
    return async (dispatch) => {
		const { id } = data;
		try {
			const params = id ? {
						url: `lessons/${id}`,
						method: 'PATCH'
					} : {
						url: 'lessons',
						method: 'POST'
					},
				course = await Api.__request({
					...params,
					data,
					type: 'content'
				});


			dispatch({
				type: SET_LESSON,
				course
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: 'Data succesfully updated!'
				}
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}


export function addSingleMaterial(data) {
    return async (dispatch) => {
		const { id } = data;
		try {
			const params = id ? {
						url: `lessons/${id}`,
						method: 'PATCH'
					} : {
						url: 'lessons',
						method: 'POST'
					},
				course = await Api.__request({
					...params,
					data,
					type: 'content'
				});


			dispatch({
				type: SET_MATERIAL,
				
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

