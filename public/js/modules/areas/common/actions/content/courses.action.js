import uniq from 'uniqid';

import { SET_WARNINGS } from '../../../../../libraries/warnings/constant/warnings.const.js';
import {
	GET_COURSES,
	SET_COURSE,
	REMOVE_COURSES,
	GET_COURSE_USERS
} from '../../constants/content.const.js';
import Api from '../../../../../service/api.js';

import { setDefaultLimit } from '../../../../../helpers/filters/limit.helper.js';
import { dataURLtoFile } from '../../../../../helpers/base64ToBlob.helper.js';

export function getCourseList(filters) {
    return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				corses = await Api.__request({
					url: `courses/${flt ? `?${flt}` : ''}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_COURSES,
				corses,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function getSingleCourse(id) {
    return async (dispatch) => {
		try {
			const course = await Api.__request({
					url: `courses/${id}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: SET_COURSE,
				course
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function removeSingleCourse(id) {
    return async (dispatch) => {
		try {
			await Api.__request({
					url: `courses/${id}`,
					method: 'DELETE',
					type: 'content'
				});

			dispatch({
				type: REMOVE_COURSES,
				id
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function addOrEditCourse(data) {
    return async (dispatch) => {
		const { id, titleImageUrl } = data;
		try {
			if (titleImageUrl && Array.isArray(titleImageUrl)) {
				const image = dataURLtoFile(titleImageUrl, 'single'),
					  formData = new FormData();

				formData.append(uniq('upload-'), image);

				const { filePath } = await Api.__request({
					url: 'upload_file',
					method: 'FILE',
					data: formData,
					type: 'file'
				});

				data.titleImageUrl = filePath;
			}

			const params = id ? {
						url: `courses/${id}`,
						method: 'PATCH'
					} : {
						url: 'courses',
						method: 'POST'
					},
				course = await Api.__request({
					...params,
					data,
					type: 'content'
				});


			dispatch({
				type: SET_COURSE,
				course
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: 'Data succesfully updated!'
				}
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function getNotInCourse(id, type, filters) {
	return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				users = await Api.__request({
				url: `/courses/${id}/not_assigned_users/`,
				method: 'GET',
				type: 'content'
			});

			dispatch({
				type: GET_COURSE_USERS,
				users
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function getInCourse(id, type, filters) {
	return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				users = await Api.__request({
					url: `/courses/${id}/assigned_users/`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_COURSE_USERS,
				users
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function addCourseToUser(uid, cid) {
	return async (dispatch) => {
		try {
			await Api.__request({
					url: `course/add_user`,
					method: 'POST',
					type: 'content',
					data: {courseId: cid, uid}
				});

			const users = await Api.__request({
					url: `courses/${cid}/not_assigned_users/`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_COURSE_USERS,
				users
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function removeCourseToUser(uid, cid) {
	return async (dispatch) => {
		try {
			await Api.__request({
				url: `course/remove_user`,
				method: 'POST',
				type: 'content',
				data: {courseId: cid, uid}
			});

			const users = await Api.__request({
					url: `courses/${cid}/assigned_users/`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_COURSE_USERS,
				users
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function getMyCourseList(filters) {
	return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				corses = await Api.__request({
					url: `my_courses/${flt ? `?${flt}` : ''}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_COURSES,
				corses,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}
