import uniq from 'uniqid';

import { SET_WARNINGS } from '../../../../../libraries/warnings/constant/warnings.const.js';
import {
	GET_MODULES,
	SET_MODULES,
	REMOVE_MODULES
} from '../../constants/content.const.js';

import Api from '../../../../../service/api.js';

import { setDefaultLimit } from '../../../../../helpers/filters/limit.helper.js';
import { dataURLtoFile } from '../../../../../helpers/base64ToBlob.helper.js';

export function getModuleByCourse(filters, cid) {
    return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				modules = await Api.__request({
					url: `courses/${cid}/modules/${flt ? `?${flt}` : ''}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: GET_MODULES,
				modules,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}


export function getSingleModule(id) {
    return async (dispatch) => {
		try {
			const module = await Api.__request({
					url: `modules/${id}`,
					method: 'GET',
					type: 'content'
				});

			dispatch({
				type: SET_MODULES,
				module
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function removeSingleModule(id) {
    return async (dispatch) => {
		try {
			await Api.__request({
					url: `modules/${id}`,
					method: 'DELETE',
					type: 'content'
				});

			dispatch({
				type: REMOVE_MODULES,
				id
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function addOrEditModule(data) {
    return async (dispatch) => {
		const { id, titleImageUrl } = data;
		try {
			if (titleImageUrl && Array.isArray(titleImageUrl)) {
				const image = dataURLtoFile(titleImageUrl, 'single'),
					  formData = new FormData();

				formData.append(uniq('upload-'), image);

				const { filePath } = await Api.__request({
					url: 'upload_file',
					method: 'FILE',
					data: formData,
					type: 'file'
				});

				data.titleImageUrl = filePath;
			}

			const params = id ? {
						url: `modules/${id}`,
						method: 'PATCH'
					} : {
						url: 'modules',
						method: 'POST'
					},
				module = await Api.__request({
					...params,
					data,
					type: 'content'
				});


			dispatch({
				type: SET_MODULES,
				module
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: 'Data succesfully updated!'
				}
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}
