import { SET_WARNINGS } from '../../../../../libraries/warnings/constant/warnings.const.js';
import {
	SET_LESSON
} from '../../constants/content.const.js';
import Api from '../../../../../service/api.js';

export function addOrEditMaterials(data, lid) {
    return async (dispatch) => {
		const { sid } = data;
		try {
			const params = sid ? {
						url: `lessons/${lid}/materials/${sid}`,
						method: 'PATCH'
					} : {
						url: `lessons/${lid}/materials`,
						method: 'POST'
					},
				lesson = await Api.__request({
					...params,
					data,
					type: 'content'
				});

			dispatch({
				type: SET_LESSON,
				lesson
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}