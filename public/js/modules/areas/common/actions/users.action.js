import { SET_WARNINGS } from '../../../../libraries/warnings/constant/warnings.const.js';
import {
	GET_USERS,
	GET_GROUPS,
	INVITE_USER,
	CREATE_NEW_GROUP,
	REMOVE_GROUP,
	UPDATE_GROUP
} from '../constants/users.const.js';
import Api from '../../../../service/api.js';

import { setDefaultLimit } from '../../../../helpers/filters/limit.helper.js';

export function getGroupList(filters) {
    return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				groups = await Api.__request({
					url: `groups/${flt ? `?${flt}` : ''}`,
					method: 'GET',
					type: 'users'
				});

			dispatch({
				type: GET_GROUPS,
				groups,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function getUserlList(filters) {
    return async (dispatch) => {
		try {
			const flt = setDefaultLimit(filters),
				users = await Api.__request({
					method: 'GET',
					url: `company/users/${flt ? `?${flt}` : ''}`,
					type: 'users'
				});

			dispatch({
				type: GET_USERS,
				users,
				filters
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function inviteUser(data) {
    return async (dispatch) => {
		try {
			const { url: type, email } = data,
					url = type === 'employee' ? 'invite/employee' : 'invite/customer';

			data.service = 'educational';
			await Api.__request({
				url,
				method: 'POST',
				data,
				type: 'login'
			});

			dispatch({
				type: INVITE_USER,
				user: email
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: `We have sent invitation to this email ${email}`
				}
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function addGroup(data) {
    return async (dispatch) => {
		try {
			data.service = 'educational';
			const group = await Api.__request({
				url: 'groups',
				method: 'POST',
				data,
				type: 'users'
			});

			dispatch({
				type: CREATE_NEW_GROUP,
				group
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function updateGroup(data) {
    return async (dispatch) => {
		try {
			const { gid } = data;

			data.service = 'educational';
			const group = await Api.__request({
				url: `groups/${gid}`,
				method: 'PATCH',
				data,
				type: 'users'
			});

			dispatch({
				type: UPDATE_GROUP,
				group
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}

export function removeGroup(gid) {
    return async (dispatch) => {
		try {
			await Api.__request({
				url: `groups/${gid}`,
				method: 'DELETE',
				type: 'users'
			});

			dispatch({
				type: REMOVE_GROUP,
				gid
			});
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
    };
}