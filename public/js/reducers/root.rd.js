import { combineReducers } from 'redux';

// Modules for combine reducers
import { reducer as realTimeForm } from 'redux-form';
import oauth from '../modules/oauth/reducers/oauth.rd.js';
import warnings from '../libraries/warnings/reducers/index.rd.js';
import content from '../modules/areas/common/reducers/content.rd.js';
import users from '../modules/areas/common/reducers/users.rd';

const root = combineReducers({
    oauth,
    users,
	warnings,
	content,
    form: realTimeForm
}),
initialState = root({}, {});

export default (state, action) => {
    if (action.type === 'LOG_OUT') {
        localStorage.removeItem('authToken');
        state = initialState;
    }
    return root(state, action);
};
