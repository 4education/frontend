import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import {
	object,
	oneOfType
} from 'prop-types';

// Decorators
import autentification from '../modules/oauth/decorators/autentification.dec.jsx';

// Components
import Error from '../common/components/error/error.jsx';
import MissRoles from '../common/components/error/missRoles.jsx';

function mapStateToProps(state) {
    return {
        ...state
    };
}

@withRouter
@connect(mapStateToProps)
@autentification
class Main extends React.Component {
	static propTypes = {
		oauth: oneOfType([object])
	}

	static defaultProps = {
		oauth: {}
	}

	constructor(props) {
		super(props);
		this.state = {
			routes: null,
			component: null
		};
	}

	componentDidMount() {
		this.getRoutesByRole();
	}

	async getRoutesByRole() {
		const {
			oauth: {
				user: {
					role
				} = {}
			}
		} = this.props;
		let rt = null;

		switch (role) {
			case 'c_admin':
				rt = await import('../modules/areas/routes/company.route.js');
				break;
			case 'c_employee':
				rt = await import('../modules/areas/routes/employee.route.js');
				break;
			case 'customer':
				rt = await import('../modules/areas/routes/user.route.js');
				break;
			default:
				break;
		}

		if (rt) {
			const mixed = await import('../modules/oauth/routes/logout.router.js');

			this.setState(prevState => ({
				...prevState,
				routes: [
					...rt.routes,
					...mixed.logout
				]
			}));
		} else {
			this.setState(prevState => ({
				...prevState,
				component: MissRoles
			}));
		}
	}

    render() {
		const { routes, component: Component } = this.state;

		if (!routes && !Component) {
			return null;
		}

		if (!routes && Component) {
			return <Component />;
		}

        return (
            <Switch>
                {
                    routes && routes.length && routes.map((item) => {
						const { path, component } = item;
                        return (
                            <Route key={path} exact path={path} component={component} />
                        );
                    })
                }
                <Route component={Error} />
            </Switch>
        );
    }
}

export default Main;
