const {
	NODE_ENV,
	USERS_SERVICE_HOST_REST,
	USERS_SERVICE_PORT,
	LOGIN_HOST,
	LOGIN_PORT,
	CONTENT_SERVICE_HOST,
	CONTENT_SERVICE_PORT,
	FILE_STORAGE_SERVICE_HOST,
	FILE_STORAGE_SERVICE_PORT
} = process.env,

configuration = {
	protocol: NODE_ENV === 'production' ? 'https://' : 'http://',
	userService: `${USERS_SERVICE_HOST_REST}${USERS_SERVICE_PORT ? `:${USERS_SERVICE_PORT}` : ''}`,
	loginService: `${LOGIN_HOST}${LOGIN_PORT ? `:${LOGIN_PORT}` : ''}`,
	contentService: `${CONTENT_SERVICE_HOST}${CONTENT_SERVICE_PORT ? `:${CONTENT_SERVICE_PORT}` : ''}`,
	fileStorageService: `${FILE_STORAGE_SERVICE_HOST}${FILE_STORAGE_SERVICE_PORT ? `:${FILE_STORAGE_SERVICE_PORT}` : ''}`
};

export default configuration;
