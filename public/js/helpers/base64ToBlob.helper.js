import uniq from 'uniqid';

export const dataURLtoFile = (dataurl, type) => {
	const images = Array.isArray(dataurl) ? dataurl : [dataurl],
		  files = [];

	for (let index = 0; index < images.length; index++) {
		const element = images[index],
			arr = element.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]);

		let n = bstr.length;
		const u8arr = new Uint8Array(n);

		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}

		files.push(new File([u8arr], uniq('images-'), { type: mime }));
	}

	switch (type) {
		case 'single':
			return _.head(files);
		default:
			return files;
	}
}