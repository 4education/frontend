import { parse, stringify } from '../queryObject.helper.js';
import rules from '../../configuration/rules.json';

export const normalizeOffset = (filters = '') => {
    const jsonFilters = parse(filters),
          offsetValue = jsonFilters.offset || 0,
          limitValue = jsonFilters.limit || rules.limit;

    return Math.ceil(offsetValue / limitValue);
};

export const setDefaultLimit = (filters, limit) => {
    const jsonFilters = parse(filters);

    jsonFilters.limit = limit || rules.limit;

    return stringify({
        ...jsonFilters
    });
};

export const normalizeOffsetWithPageLimit = (filters = '', pageNumber) => {
    const jsonFilters = parse(filters),
          limitValue = jsonFilters.limit || rules.limit;

    return Math.ceil(pageNumber * limitValue);
};

export const mergeOffsetWithFilters = (filters = '', value) => {
    const jsonFilters = parse(filters),
          offsetValue = jsonFilters.offset;

    if (offsetValue && offsetValue === value) return null;

    delete jsonFilters.offset;

    const offset = value ? { offset: value * rules.limit } : {};

    return stringify({
        ...jsonFilters,
        ...offset
    });
};

export const denormalizePages = (filters = '', count = 0) => {
    const jsonFilters = parse(filters),
          limit = jsonFilters.limit || rules.limit;

    return Math.ceil(count / limit);
};
