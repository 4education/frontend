import queryObject from '../queryObject.js';

const rules = {
	search: 'equally',
	label: 'equally',
	gid: 'in',
	pastDate: 'range',
	score: 'range'
},

stringToJson = (filters) => {
	const newValues = { ...filters },
		  currentFilters = newValues.filter || '',
		  jsonValue = currentFilters.isJSON() ? JSON.parse(currentFilters) : {};

	return jsonValue;
};

export const changeOneOfFilter = (filters = '', obj) => {
	const jsonFilters = queryObject.parse(filters),
		  newValues = { ...jsonFilters },
		  currentFilters = newValues.filter;

	try {
	  const parsed = JSON.parse(currentFilters),
			newFilters = { ...parsed, ...obj };

		  return queryObject.stringify({
			  ...jsonFilters,
			  filter: JSON.stringify(newFilters)
		  });
	} catch (e) {
		return queryObject.stringify({
			...jsonFilters,
			filter: JSON.stringify(obj)
		});
	}
};

export const prepareFilters = (obj, filters = '', removed) => {
	const jsonFilters = (filters && queryObject.parse(filters)) || {},
		  methods = {
			equally(val, key) {
				if (val) {
					jsonValue[key] = val;
				} else {
					delete jsonValue[key];
				}
			},
			in(val, key) {
				if (val) {
					const vl = Array.isArray(val) ? val : [val];
					jsonValue[key] = { in: vl };
				} else {
					delete jsonValue[key];
				}
			},
			range(value, key) {
				const [from, to] = value;
				if(!from || !to) {
					delete jsonValue[key];
					return;
				}

				jsonValue[key] = {">=": from, "<=": to};
			}
		};

	let jsonValue = stringToJson(jsonFilters);
	
	if(Object.keys(obj).length > 0) {
		for (const variable in obj) {
			if (obj.hasOwnProperty(variable) && rules[variable] && methods[rules[variable]]) {
				methods[rules[variable]](obj[variable], variable);
			}
		}
	} else {
		jsonValue = {};
	}
	

	if (removed && removed.length) {
		for (const variable of removed) {
			delete jsonFilters[variable];
		}
	}

	delete jsonFilters.filter;

	console.log()

	const prepared = Object.keys(jsonValue).length ? { filter: JSON.stringify({ ...jsonValue }) } : { };

	return queryObject.stringify({
		...jsonFilters,
		...prepared
	});
};

export const getOneOf = (filters = '', name) => {
	const jsonFilters = queryObject.parse(filters),
		  filtersObject = jsonFilters.filter || {};
	try {
		const parsed = JSON.parse(filtersObject);
		return parsed[name];
	} catch (e) {
		return '';
	}
};


export const changeOneOf = (filters = '', obj) => {
	const jsonFilters = queryObject.parse(filters),
		  newValues = { ...jsonFilters },
		  currentFilters = newValues.filter;

	try {
	  const parsed = JSON.parse(currentFilters),
			newFilters = { ...parsed, ...obj };

		  return queryObject.stringify({
			  ...jsonFilters,
			  filter: JSON.stringify(newFilters)
		  });
	} catch (e) {
		return queryObject.stringify({
			...jsonFilters,
			filter: JSON.stringify(obj)
		});
	}
};

export const destructionFilters = (filters) => {
	const jsonFilters = (filters && queryObject.parse(filters)) || {},
		  jsonValue = stringToJson(jsonFilters),
		  methods = {
			  equally(value, key) {
				  return {
					  [key]: value
				  };
			  },
			  in(value, key) {
				  return { [key]: value.in };
			  },
			  range(value, key) {
				const {">=": from, "<=": to} = value;
				return { [key]: [from, to] };
			}
		  };

	let normalized = {};

	_.forOwn(jsonValue, (value, key) => {
		const name = rules[key],
			  action = name && methods[name];

		if (action) {
			normalized = { ...normalized, ...action(value, key) };
		}
	});

	return normalized;
};

export const removeFilters = (arr, filters, removed) => {
	const jsonFilters = queryObject.parse(filters),
		  { filter } = { ...jsonFilters };

	  try {
			const parsed = filter && JSON.parse(currentFilters);

			if(arr && arr.length) {
				for (const variable of arr) {
					delete parsed[variable];
				}
			}

			if (removed && removed.length) {
				for (const variable of removed) {
					delete jsonFilters[variable];
				}
			}

			delete jsonFilters.filter;

			const prepared = parsed && Object.keys(parsed).length ? { filter: JSON.stringify({ ...parsed }) } : { };

			return queryObject.stringify({
				...jsonFilters,
				...prepared
			});
	  } catch (e) {
		  return queryObject.stringify({
			  ...jsonFilters
		  });
	  }
};
