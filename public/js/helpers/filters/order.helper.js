import queryObject from '../queryObject.js';

export const ordering = (value) => {
    switch (value) {
        case 'ask':
            return 'desc';
        case 'desc':
            return 'ask';
        default:
            return 'ask';
    }
};

export const orderingByClass = (value) => {
    switch (value) {
        case 'ask':
            return 'ask';
        case 'desc':
            return 'desc';
        default:
            return 'desc';
    }
};

export const changeOneOfOrder = (filters, obj) => {
    const jsonFilters = queryObject.parse(filters),
          newValues = { ...jsonFilters },
          currentFilters = newValues.orderBy;

      try {
        const parsed = JSON.parse(currentFilters),
            newOrderBy = { ...parsed, ...obj };

            return queryObject.stringify({
                ...jsonFilters,
                orderBy: JSON.stringify(newOrderBy)
            });
      } catch (e) {
          return queryObject.stringify({
              ...jsonFilters,
              orderBy: JSON.stringify(obj)
          });
      }
};

export const changeOneOfFilter = (filters = '', obj) => {
    const jsonFilters = queryObject.parse(filters),
          newValues = { ...jsonFilters },
          currentFilters = newValues.filters;

    try {
      const parsed = JSON.parse(currentFilters),
          newFilters = { ...parsed, ...obj };

          return queryObject.stringify({
              ...jsonFilters,
              filters: JSON.stringify(newFilters)
          });
    } catch (e) {
        return queryObject.stringify({
            ...jsonFilters,
            filters: JSON.stringify(obj)
        });
    }
};

export const orderBy = (filters) => {
    if (!filters) return {};

    const jsonFilters = queryObject.parse(filters);

    if (jsonFilters && jsonFilters.orderBy) {
        try {
            return JSON.parse(jsonFilters.orderBy);
        } catch (e) {
            return {};
        }
    }
    return {};
};

export const normalizeObderBy = (filters = '', name, value) => {
    const jsonFilters = queryObject.parse(filters);

      if (!name) return false;

      return queryObject.stringify({
          ...jsonFilters,
          orderBy: JSON.stringify({ [name]: ordering(value) })
      });
};
