export const isBase64 = (str) => {
	if (str === '' || str.trim() === '') { return false; }
	const trimed = str.split(',');

	let ns = str;

	if (trimed[1]) {
		ns = trimed[1];
	}

    try {
        return btoa(atob(ns)) === ns;
    } catch (err) {
        return false;
    }
};
